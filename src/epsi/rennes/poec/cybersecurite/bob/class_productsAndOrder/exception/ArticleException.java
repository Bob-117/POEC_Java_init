package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.exception;


public class ArticleException extends Exception {

    public static void checkPrice(float price) throws Exception {
        if (price <= 0) {
            System.out.println("prix negatif");
            throw new Exception();
        }
    }
}