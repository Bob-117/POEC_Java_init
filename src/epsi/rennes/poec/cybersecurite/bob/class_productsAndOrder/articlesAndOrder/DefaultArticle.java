package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder;


public class DefaultArticle extends Article {
    private String code;
    private String label;
    private float price;

    public DefaultArticle(String code, float price) throws Exception {
        super(code, "default label", price);
    }

    @Override
    public void show(){
        System.out.print("Default article :  ");
        System.out.print(this.getCode());
        System.out.print("  :  ");
        System.out.print(this.getLabel());
        System.out.print("  :  ");
        System.out.print(this.getPrice());
        System.out.println("€");
    }
}
