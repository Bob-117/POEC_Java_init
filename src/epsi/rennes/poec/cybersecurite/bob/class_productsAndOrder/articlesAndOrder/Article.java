package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder;
import epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.exception.ArticleException;
import epsi.rennes.poec.cybersecurite.bob.constants.Const;

import java.util.Random;

public class Article {

    private String code;
    private String label;
    private float price;

    // ========== CONSTRUCTOR ==========

    public Article(String code, String label, float price) throws Exception {
        this.code = code;
        this.label = label;
        ArticleException.checkPrice(price);
        this.price = price;
    }

    // ========== GETTER ==========
    public String getCode() {
        return  this.code;
    }

    public String getLabel() {
        return this.label;
    }

    public float getPrice() {
        return this.price;
    }

    // ========== SETTER ==========
    public boolean setCode(String new_code) {
        this.code = new_code;
        return true;
    }

    public boolean setLabel(String new_label) {
        this.label = new_label;
        return true;
    }

    public boolean setPrice(float new_price) {
        this.price = new_price;
        return true;
    }

    // ========== DEBUG ==========

    public void show(){
        System.out.print(this.getCode());
        System.out.print("  :  ");
        System.out.print(this.getLabel());
        System.out.print("  :  ");
        System.out.print(this.getPrice());
        System.out.println("€");
    }
    // ========== STATIC ==========
    public static Article createArticle() {

        String[] names = Const.names;

        Random rPrice = new Random();
        float randPrice = rPrice.nextInt((100 - 10) + 1) + 10;

        Random rName = new Random();
        int randName = rName.nextInt(names.length);
        String name = names[randName];

        Article article = null;
        try {
            article = new Article("code", name, randPrice);

        } catch(Exception e){
            e.printStackTrace();
        }
        return article;
    }
}
