package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder;

import java.util.ArrayList;

public class Order{
    private final String number;
    private int nbSavedArticle = 0;
    private final ArrayList<Article> articleArray;

    public Order(String number) {
        this.number = number;
        this.articleArray = new ArrayList<>();
    }


    public void addOne(Article article){
        if (article != null) {
            this.addInList(article);
        }
    }

    private void addInList(Article article){
        // this.articleArray[this.nbSavedArticle] = article;
        this.articleArray.add(article);
        this.nbSavedArticle += 1;
    }

    public float totalPrice(){
        float sum = 0;
        for (Article article : this.articleArray){
            if (article != null){
                sum += article.getPrice();
            }
        }
        return sum;
    }

    public float avgPrice(){
        return this.totalPrice() / this.nbSavedArticle;
    }

    public void showList(){
        System.out.println("==========LIST=========");
        /**int i = 1;
        for (Article a : this.articleArray) {
            System.out.print(i);
            System.out.print("  :  ");
            System.out.print(a.get_label());
            System.out.print("  :  ");
            System.out.println(a.get_price());
            i++;
        }**/
        if (this.articleArray.size() != 0) {
            for (int i = 0; i < this.nbSavedArticle; i++) {
                Article currentArticle = this.articleArray.get(i);
                System.out.print(i);
                System.out.print("  :  ");
                System.out.print(currentArticle.getLabel());
                System.out.print("  :  ");
                System.out.print(currentArticle.getPrice());
                System.out.println("€");
            }
        }
        else {
            System.out.println("Empty list");
        }
        System.out.println("======================");
    }

    public void showSumAvg(){
        float total = this.totalPrice();
        float avg = this.avgPrice();
        System.out.println("Order n°" + this.number + " => Total : " + total + ", Avg : " + avg);
    }

    public void show(){
        this.showSumAvg();
        this.showList();

    }

}
