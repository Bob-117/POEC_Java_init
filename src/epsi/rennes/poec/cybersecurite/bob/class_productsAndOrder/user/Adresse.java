package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.user;

public class Adresse {

    private int number;
    private String street;
    private String city;
    private int postcode;

    // ========== Constructor ==========


    public Adresse(int number, String street, String city, int postcode) {
        this.number = number;
        this.street = street;
        this.city = city;
        this.postcode = postcode;
    }

    // ========== GETTER ==========
    public int getNumber() {
        return this.number;
    }

    public String getStreet() {
        return this.street;
    }

    public String getCity() {
        return this.city;
    }

    public int getPostcode() {
        return this.postcode;
    }

    // ========== SETTER ==========
    public boolean set_number(int new_number) {
        this.number = new_number;
        return true;
    }

    public void setNumber(int new_number) {
        this.number = new_number;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }
}
