package epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.user;

import java.time.LocalDate;

public class User {
    private String first_name;
    private String last_name;
    private Adresse address;
    private int birthYear;

    public User(String first_name, String last_name, Adresse address, int birthYear)
    {
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.birthYear = birthYear;
    }

    public User()
    {

    }

    public User(int birthYear)
    {
        this.birthYear = birthYear;
    }

    // ========== GETTER ==========

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Adresse getAddress() {
        return address;
    }

    public int getBirthYear() {
        return birthYear;
    }

    // ========== SETTER ==========

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setAddress(Adresse address) {
        this.address = address;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    // ========== METHODS ==========
    public int getAge()
    {
        return  LocalDate.now().getYear() - this.birthYear;
    }

    public void hello()
    {
        System.out.println("Bonjour je m'appelle " + this.getFirst_name());
    }

    public void fullName()
    {
        System.out.println("Mon nom complet est  " + this.getFirst_name() + " " + this.getLast_name());
    }

}
