package epsi.rennes.poec.cybersecurite.bob.app;

import epsi.rennes.poec.cybersecurite.bob.class_cars.Car;
import epsi.rennes.poec.cybersecurite.bob.class_cars.CarABS;

import epsi.rennes.poec.cybersecurite.bob.class_companyAndWorkers.Company;
import epsi.rennes.poec.cybersecurite.bob.class_companyAndWorkers.Worker;

import epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder.Article;
import epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder.Order;
import epsi.rennes.poec.cybersecurite.bob.class_productsAndOrder.articlesAndOrder.DefaultArticle;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws Exception {

        companyAndWorkers_Storyboard();
        cars_Storyboard();
        articlesAndOrder_storyboard();



    }

    /**
     * Company and Workers class test
     */
    public static void companyAndWorkers_Storyboard() {

        System.out.println("-----------------Company & Workers-------------------");
        Company myCompany = new Company("EPSI");
        System.out.println(myCompany.getName());

        Worker bob = new Worker("Bob", "Gibson", 1000);
        myCompany.hireWorker(bob);
        myCompany.showWorkers();

        for (int i = 0; i < 5; i++){
            Worker worker = Worker.createWorker();
            myCompany.hireWorker(worker);
        }
        myCompany.showAll();
        Worker newIntern = new Worker("Intern", "Poec", 1000);
        myCompany.hireWorker(newIntern);
        myCompany.showAll();

        System.out.println("-----------------END-------------------");
        System.out.println();
        System.out.println();
    }

    /**
     * Cars class test
     */
    public static void cars_Storyboard() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        System.out.println("-----------------------Cars-------------------------");

        Car carWithoutABS = new Car("blue", 0);
        CarABS carAbs = new CarABS("red", 0);
        System.out.println(carWithoutABS.getColor());
        System.out.println(carAbs.getColor());

        drive(Car.class.getMethod("speedUp"), carWithoutABS, carAbs, 3);
        drive(Car.class.getMethod("slowDown"), carWithoutABS, carAbs, 7);

        System.out.println("-----------------END-------------------");
        System.out.println();
        System.out.println();
    }

    public static void drive(Method function, Car car1, Car car2, int time) throws InvocationTargetException, IllegalAccessException {
        for (int i = 0; i < time; i++){
            System.out.println(
                    car1.getColor() + " : " + car1.getSpeed() + "    " +
                            car2.getColor() + " : " + car2.getSpeed());
            function.invoke(car1);
            function.invoke(car2);
        }
    }

    public static void articlesAndOrder_storyboard() throws Exception {

        System.out.println("-----------------Articles & Order-------------------");

        Article art = new Article("abc", "test", 117);
        Order order = new Order("314159265");
        DefaultArticle dftart = new DefaultArticle("code", 117);

        art.show();
        dftart.show();
        order.show();

        order.addOne(art);
        order.addOne(dftart);
        order.show();

        for (int i = 0 ; i < 5 ; i++) {
            Article newArticle = Article.createArticle();
            order.addOne(newArticle);
        }
        order.show();

        System.out.println("-----------------END-------------------");
        System.out.println();
        System.out.println();
    }

}