package epsi.rennes.poec.cybersecurite.bob.class_companyAndWorkers;

import java.util.Random;
import epsi.rennes.poec.cybersecurite.bob.constants.Const;

public class Worker {
    private String firstName;
    private String lastName;
    private float wage;
    private String company;

    public Worker(String firstName, String lastName, float wage) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.wage = wage;
        this.company = "Jobless";
    }

    // =============== GETTER ===============
    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public double getWage() {
        return this.wage;
    }

    public String getCompany() {
        return this.company;
    }

    // =============== SETTER ===============
    public void setFirstName(String newFirstName) {
        this.firstName = newFirstName;
    }

    public void setLastName(String newLastName) {
        this.lastName = newLastName;
    }

    public void setWage(float newWage) {
        this.wage = newWage;
    }

    public void setCompany(String newCompany) {
        this.company = newCompany;
    }

    // =============== METHODS ===============

    public StringBuilder show(int index){
        StringBuilder output = new StringBuilder();
        output.append(index);
        output.append("  :  ");
        output.append(this.getFirstName() + " " + this.getLastName());
        output.append("  :  ");
        output.append(String.format("%.2f", this.getWage()) + "€");
        return output;

    }


    // =============== STATIC ===============
    public static Worker createWorker(){
        Random rWage = new Random();
        // float randWage = rWage.nextInt((3000 - 1000) + 1);
        float randWage = (float)Math.random() * (3000 - 1000) + 1000;

        Random rName = new Random();
        int randName = rName.nextInt(Const.names.length);
        String name = Const.names[randName];

        return new Worker(name, "Poec", randWage);

    }

}
