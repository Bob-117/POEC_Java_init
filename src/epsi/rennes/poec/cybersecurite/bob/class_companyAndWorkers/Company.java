package epsi.rennes.poec.cybersecurite.bob.class_companyAndWorkers;

import java.util.ArrayList;
import java.util.List;


/**
 * Company class
 * @params String name, ArrayList of workers
 */
public class Company {
    private String name;
    private List<Worker> workers = new ArrayList<>();

    // =============== CONSTRUCTOR ===============
    public Company(String name){
        this.name = name;
    }

    // =============== GETTER ===============
    public String getName() {
        return name;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    // =============== SETTER ===============
    public void setName(String newName) {
        this.name = newName;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    // =============== METHODS ===============
    /**
    * Method hireWorker(), add a worker in ArrayList workers
    * @param
    **/
    public void hireWorker(Worker worker){
        worker.setCompany(this.getName());
        this.workers.add(worker);
    }

    /**
     * Method showWorkers(), show every worker from workers in shell
     **/
    public void showWorkers(){
        StringBuilder output = new StringBuilder();
        int i = 1;
        for (Worker w : this.workers){
            System.out.println(w.show(i));

            i++;
        }
        System.out.println(output);
    }

    /**
     * Method sumWage(), return total company wage bill
     * @return float sum
     */
    public float sumWage(){
        float sum = 0;
        for (Worker w : this.workers){
            sum += w.getWage();
        }
        return sum;
    }
    /**
     * Method avgWage(), return average company wage, use sumWage()
     * @return float avg
     */
    public float avgWage(){
        return this.sumWage() / workers.size();
    }

    public void showSumAvg(){
        float sum = this.sumWage();
        float avg = this.avgWage();
        System.out.println("Entreprise : " + this.name + " => Masse salariale : " + sum + ", Moyenne : " + avg);
    }

    public void showAll(){
        System.out.println("**********************");
        this.showSumAvg();
        this.showWorkers();
        System.out.println("**********************");
    }
}
