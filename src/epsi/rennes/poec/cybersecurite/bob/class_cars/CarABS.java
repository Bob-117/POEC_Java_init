package epsi.rennes.poec.cybersecurite.bob.class_cars;

public class CarABS extends Car{

    // ========== CONSTRUCTOR ==========
    public CarABS(String color, int speed) {
        super(color, speed);
    }
    // ========== GETTER ==========
    @Override
    public String getColor() {
        return "Car color = " + super.getColor();
    }

    @Override
    public int getSpeed() {
        return super.getSpeed();
    }

    // ========== METHODS ==========
    @Override
    public void slowDown(){
        if (!this.blockedWheels()) {
            super.slowDown();
        }
    }

    protected boolean blockedWheels(){
        return this.speed <= 0;
    }
}
