package epsi.rennes.poec.cybersecurite.bob.class_cars;

public class Car {
    private String color;
    protected int  speed;

    // ========== CONSTRUCTOR ==========
    public Car(String color, int speed) {
        this.color = color;
        this.speed = speed;
    }

    // ========== GETTER ==========
    public String getColor() {
        return this.color;
    }

    public int getSpeed() {
        return this.speed;
    }

    // ========== METHODS ==========
    public void speedUp(){
        this.speed++;
    }

    public void slowDown(){
        --this.speed;
    }

}
