package epsi.rennes.poec.cybersecurite.bob.constants;

public class Const {

    public static final String[] names = new String[]{
            "Python", "Javascript",
            "Json", "Markdown", "Java", "Html",
            "Css", "Vuejs", "Numpy"
    };

    public static final String[] products = new String[]{
            "guitar", "gibson SG", "gibson Explorer",
            "fender Telecaster", "fender Stratocaster",
            "pick", "guitar string", "6.35jack"
    };
}
