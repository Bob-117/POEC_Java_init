# POEC CYBERSECURITE 2022 - EPSI Rennes

### 24 mars 2022 / 26 juin 2022

## Exercices d'entrainement au developpement Java

### TODO :

- [X] Une entreprise et des employés 
  - Gestion des listes et des attributs
- [x] Une commande et des produits 
  - Une classe DefaultArticle extends Article avec un attribut par defaut
  - Gestion des exceptions custom 
    - Gestion du prix négatif d'un produit
- [x] Des voitures avec et sans ABS 
  - Hérédité, protected, override
  - invoke, exceptions automatiques
- [ ] Ecriture et copie d'un fichier
  - byte par byte, ligne par ligne
  - gestion des exceptions try{}catch(), try{}finally()